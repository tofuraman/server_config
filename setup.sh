#!/usr/bin/env bash

IP=$(ifconfig | grep 192.168.0 | awk '{print $2}')
HOSTNAME=$(hostname)
BUILDBOT_PASS=${BUILDBOT_PASS:-"mtolman-buildbot-pass"}

common_setup() {
    sudo apt-get update
    sudo apt-get install -y net-tools git build-essential uidmap python3 python3-pip
}

##########################
## Docker Setup
##########################


docker_setup() {
    common_setup
    command -v docker && return
    printf '\nInstalling docker...\n'
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    rm get-docker.sh
    dockerd-rootless-setuptool.sh install
    echo 'export PATH=/usr/bin:$PATH' >> ~/.bashrc
    echo 'export DOCKER_HOST=unix:///run/user/1000/docker.sock'  >> ~/.bashrc
    source ~/.bashrc
    sudo apt-get install -y docker-compose
    printf '\nDocker Installed!...\n'
}


##########################
## Jupyter Setup
##########################

jupyter_setup() {
  sudo apt-get install -y python3 python3-pip

  sudo adduser \
    --system \
    --shell /bin/bash \
    --gecos 'Nexus Repository' \
    --group \
    --disabled-password \
    jupyter
  
  sudo su -c "pip3 install jupyterlab" jupyter

  sudo mkdir -p /var/jupyter
  sudo chown -R jupyter /var/jupyter
  sudo chmod -R a+rw /var/jupyter

  echo "c.ServerApp.ip = '*'
c.ServerApp.port = 9650
c.ServerApp.password  = u'sha1:e0501acb1912:14835b98a7d800d4b335a710262e6518f04ac661'
c.ServerApp.open_browser = False
" > jupyter_lab_config.py
  sudo mkdir -p /home/jupyter/.jupyter
  sudo mv jupyter_lab_config.py /home/jupyter/.jupyter/
  sudo chown jupyter -R /home/jupyter/.jupyter
  sudo chmod -R +rw /home/jupyter/.jupyter

  echo "[Unit]
Description=Jupyter Labs
After=network.target

[Service]
Type=simple
ExecStart=/home/jupyter/.local/bin/jupyter-lab
KillSignal=SIGINT
User=jupyter
Restart=on-abort
WorkingDirectory=/var/jupyter

[Install]
WantedBy=multi-user.target" > jupyter.service
    sudo mv jupyter.service /etc/systemd/system
    sudo systemctl enable jupyter.service
    sudo systemctl start jupyter.service
    printf '\nJupyter Installed!...\n'
}


##########################
## Gitea Setup
##########################


gitea_setup() {
    common_setup
    printf '\nInstalling Gitea...\n'
    wget -O gitea https://dl.gitea.io/gitea/1.15.6/gitea-1.15.6-linux-amd64
    chmod +x gitea
    sudo adduser \
    --system \
    --shell /bin/bash \
    --gecos 'Git Version Control' \
    --group \
    --disabled-password \
    --home /home/git \
    git
    sudo mkdir -p /var/lib/gitea/{custom,data,log}
    sudo chown -R git:git /var/lib/gitea/
    sudo chmod -R 750 /var/lib/gitea/
    sudo mkdir /etc/gitea
    sudo chown root:git /etc/gitea
    sudo chmod 770 /etc/gitea

    sudo mv gitea /usr/local/bin/gitea
    echo "[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
###
# Don't forget to add the database service dependencies
###
#
#Wants=mysql.service
#After=mysql.service
#
#Wants=mariadb.service
#After=mariadb.service
#
#Wants=postgresql.service
#After=postgresql.service
#
#Wants=memcached.service
#After=memcached.service
#
#Wants=redis.service
#After=redis.service
#
###
# If using socket activation for main http/s
###
#
#After=gitea.main.socket
#Requires=gitea.main.socket
#
###
# (You can also provide gitea an http fallback and/or ssh socket too)
#
# An example of /etc/systemd/system/gitea.main.socket
###
##
## [Unit]
## Description=Gitea Web Socket
## PartOf=gitea.service
##
## [Socket]
## Service=gitea.service
## ListenStream=<some_port>
## NoDelay=true
##
## [Install]
## WantedBy=sockets.target
##
###

[Service]
# Modify these two values and uncomment them if you have
# repos with lots of files and get an HTTP error 500 because
# of that
###
#LimitMEMLOCK=infinity
#LimitNOFILE=65535
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
# If using Unix socket: tells systemd to create the /run/gitea folder, which will contain the gitea.sock file
# (manually creating /run/gitea doesn't work, because it would not persist across reboots)
#RuntimeDirectory=gitea
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea
# If you install Git to directory prefix other than default PATH (which happens
# for example if you install other versions of Git side-to-side with
# distribution version), uncomment below line and add that prefix to PATH
# Don't forget to place git-lfs binary on the PATH below if you want to enable
# Git LFS support
#Environment=PATH=/path/to/git/bin:/bin:/sbin:/usr/bin:/usr/sbin
# If you want to bind Gitea to a port below 1024, uncomment
# the two values below, or use socket activation to pass Gitea its ports as above
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE
###

[Install]
WantedBy=multi-user.target
    " > gitea.service

    sudo mv gitea.service /etc/systemd/system
    sudo systemctl enable gitea.service
    sudo systemctl start gitea.service

    # Post install
    # sudo chmod 750 /etc/gitea
    # sudo chmod 640 /etc/gitea/app.ini
    printf '\nGitea Installed...\n'
}


##########################
## Nexus Setup
##########################


nexus_setup() {
    common_setup
    # wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz
    printf '\nInstalling Nexus...\n'
    sudo apt-get install -y openjdk-8-jre
    sudo adduser \
    --system \
    --shell /bin/bash \
    --gecos 'Nexus Repository' \
    --group \
    --disabled-password \
    nexus
    wget https://download.sonatype.com/nexus/3/nexus-3.37.0-01-unix.tar.gz
    tar -zxf nexus-3.37.0-01-unix.tar.gz
    sudo mv nexus-3.37.0-01/ /opt/nexus/
    sudo mv sonatype-work/ /opt/sonatype-work/
    sudo chown -R nexus:nexus /opt/nexus
    sudo chown -R nexus:nexus /opt/sonatype-work
    sudo cat /opt/nexus/bin/nexus.rc > nexus.rc
    echo 'run_as_user="nexus"' >> nexus.rc
    sudo mv nexus.rc /opt/nexus/bin/nexus.rc
    echo "[Unit]
Description=nexus service
After=network.target

[Service]
Type=forking
LimitNOFILE=65536
ExecStart=/opt/nexus/bin/nexus start
ExecStop=/opt/nexus/bin/nexus stop
User=nexus
Restart=on-abort
WorkingDirectory=/opt/nexus/

[Install]
WantedBy=multi-user.target" > nexus.service
    sudo mv nexus.service /etc/systemd/system
    sudo systemctl enable nexus.service
    sudo systemctl start nexus.service
    rm nexus-3.37.0-01-unix.tar.gz
    printf '\nNexus Installed!...\n'
}


##########################
## Plex Setup
##########################


plex_setup() {
    common_setup
    printf '\nInstalling Plex...\n'
    curl -o plex.deb https://downloads.plex.tv/plex-media-server-new/1.25.8.5663-e071c3d62/debian/plexmediaserver_1.25.8.5663-e071c3d62_amd64.deb
    sudo dpkg -i plex.deb
    sudo systemctl enable plexmediaserver.service
    sudo systemctl start plexmediaserver.service
    rm plex.deb
    printf '\nPlex Installed!...\n'
}


##########################
## Compiler Explorer Setup
##########################


compiler_explorer_setup() {
  common_setup
  docker_setup

  cat << EOF > docker-compose.yml
version: "3"

services:
  compiler-explorer:
    build:
      context: ./
      dockerfile: Dockerfile
    image: madduci/docker-compiler-explorer:latest
    container_name: compiler-explorer
    ports:
      - 10240:10240
    expose:
      - 10240
    restart: on-failure
    stop_grace_period: 1m30s
EOF

cat << EOF > Dockerfile
FROM madduci/docker-linux-cpp:latest

LABEL maintainer="Michele Adduci <adduci@tutanota.com>" \
      license="Copyright (c) 2012-2021, Matt Godbolt"

EXPOSE 10240

RUN echo "*** Installing Compilers***" && \
  apt-get install -y nasm ocaml

RUN echo "*** Installing Compiler Explorer ***" \
    && DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install -y curl \
    && curl -sL https://deb.nodesource.com/setup_16.x | bash - \
    && apt-get install -y \
        wget \
        ca-certificates \
        nodejs \
        make \
        git \
    && apt-get autoremove --purge -y \
    && apt-get autoclean -y \
    && rm -rf /var/cache/apt/* /tmp/* \
    && git clone https://github.com/compiler-explorer/compiler-explorer.git /compiler-explorer \
    && cd /compiler-explorer \
    && echo "Add missing dependencies" \
    && npm i @sentry/node \
    npm run webpack

ADD cpp.properties /compiler-explorer/etc/config/c++.local.properties
ADD assembly.properties /compiler-explorer/etc/config/assembly.local.properties
ADD python.properties /compiler-explorer/etc/config/python.local.properties

WORKDIR /compiler-explorer

ENTRYPOINT [ "make" ]

CMD ["run"]
EOF

  cat << EOF > assembly.properties
  compilers=&nasm:&gnuas
compilerType=assembly
objdumper=objdump
supportsBinary=true
supportsExecute=false
demangler=

group.nasm.compilers=defaultnasm
group.nasm.versionFlag=-v

compiler.defaultnasm.name=NASM (default)
compiler.defaultnasm.exe=/usr/bin/nasm
compiler.defaultnasm.options=-f elf64 -g -F stabs

group.gnuas.compilers=defaultgnuas
group.gnuas.versionFlag=--version

compiler.defaultgnuas.name=GNU AS (default)
compiler.defaultgnuas.exe=/usr/bin/as
compiler.defaultgnuas.options=-g
EOF

  cat << EOF > python.properties
compilers=&python3

group.python3.compilers=python38
group.python3.isSemVer=true
group.python3.baseName=Python
compiler.python35.exe=python3.5
compiler.python35.semver=3.5
compiler.python36.exe=python3.6
compiler.python36.semver=3.6
compiler.python37.exe=python3.7
compiler.python37.semver=3.7
compiler.python38.exe=python3.8
compiler.python38.semver=3.8

supportsBinary=false
interpreted=true
compilerType=python
objdumper=
demangler=
postProcess=
options=
needsMulti=false
supportsExecute=true
stubText=def main():
disasmScript=
EOF

  cat << EOF > cpp.properties
# Default settings for C++
compilers=&gcc:&clang

group.gcc.compilers=g9:g10:g11:gdefault
compiler.g9.exe=/usr/bin/g++-9
compiler.g9.name=g++ 9
compiler.g10.exe=/usr/bin/g++-10
compiler.g10.name=g++ 10
compiler.g11.exe=/usr/bin/g++-11
compiler.g11.name=g++ 11
compiler.gdefault.exe=/usr/bin/g++-11
compiler.gdefault.name=g++ default (11)

group.clang.compilers=clang11:clang12:clang13:clangdefault
group.clang.intelAsm=-mllvm --x86-asm-syntax=intel
group.clang.compilerType=clang
compiler.clang11.exe=/usr/bin/clang++-11
compiler.clang11.name=clang 11
compiler.clang11x86.exe=/usr/bin/clang++-11 
compiler.clang11x86.name=clang 11 x86
compiler.clang11x86.options=-std=c++1z -Wall -Wextra -Wshadow -O3 -m32 -march=i386
compiler.clang12.exe=/usr/bin/clang++-12
compiler.clang12.name=clang 12
compiler.clang12x86.exe=/usr/bin/clang++-12
compiler.clang12x86.name=clang 12 x86
compiler.clang12x86.options=-std=c++1z -Wall -Wextra -Wshadow -O3 -m32 -march=i386
compiler.clang13.exe=/usr/bin/clang++-13
compiler.clang13.name=clang 13
compiler.clang13x86.exe=/usr/bin/clang++-13
compiler.clang13x86.name=clang 13 x86
compiler.clang13x86.options=-std=c++1z -Wall -Wextra -Wshadow -O3 -m32 -march=i386
compiler.clangdefault.exe=/usr/bin/clang++-13
compiler.clangdefault.name=clang default (13)

defaultCompiler=gdefault
postProcess=
demangler=c++filt
demanglerType=cpp
objdumper=objdump
options=
supportsBinary=true
binaryHideFuncRe=^(__.*|_(init|start|fini)|(de)?register_tm_clones|call_gmon_start|frame_dummy|\.plt.*|_dl_relocate_static_pie)$
needsMulti=false
stubRe=\bmain\b
stubText=int main(void){return 0;/*stub provided by Compiler Explorer*/}
supportsLibraryCodeFilter=true

#################################
#################################
# Installed libs (See c++.amazon.properties for a scheme of libs group)
libs=
EOF

  cat << EOF > LICENSE
Copyright (c) 2012-2021, Matt Godbolt
All rights reserved.

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright 
      notice, this list of conditions and the following disclaimer in the 
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
EOF

  sudo mkdir -p /var/compiler-explorer
  sudo chmod -R a+rw /var/compiler-explorer
  cp docker-compose.yml /var/compiler-explorer/
  cp Dockerfile /var/compiler-explorer/
  cp cpp.properties /var/compiler-explorer/
  cp assembly.properties /var/compiler-explorer/
  cp python.properties /var/compiler-explorer/
  cp LICENSE /var/compiler-explorer/
  DIR=$(pwd)

  cat << EOF > compiler-explorer.service
[Unit]
Description=Compiler Explorer Service
After=syslog.target
After=network.target

[Service]
WorkingDirectory=/var/compiler-explorer
ExecStart=/usr/bin/docker-compose up
ExecStop=/usr/bin/docker-compose down
TimeoutStartSec=2
Restart=on-failure
StartLimitIntervalSec=60
StartLimitBurst=3

[Install]
WantedBy=multi-user.target
EOF
  sudo mv compiler-explorer.service /etc/systemd/system

  cd /var/compiler-explorer || exit 1
  sudo docker-compose build
  cd "$DIR" || exit 1

  sudo systemctl enable compiler-explorer.service
  sudo systemctl start compiler-explorer.service

  printf '\nCompiler Explorer Installed!\n'
}


##########################
## Buildbot Setup
##########################


buildbot_setup() {
  common_setup
  docker_setup

cat << EOF > create-docker-compose.py

num_document_workers = 1
num_ubuntu_cmake_workers = 3
num_alpine_cmake_workers=1
num_arch_cmake_workers=1

cfg_file_text = """
import os

from buildbot.plugins import *
c = BuildmasterConfig = {}

class Worker:
  name = ''
  programs = {}
  password = ''

  def __init__(self, name, programs, password = 'pass'):
    self.name = name
    self.programs = programs
    self.password = password

  def meets_requirements(self, needed_programs = [], excluded_programs = []):
    for needed_program in needed_programs:
      if needed_program not in self.programs:
        return False
    for excluded in excluded_programs:
      if excluded in self.programs:
        return False
    return True

  def to_build_bot_worker(self):
    return worker.Worker(self.name, self.password)

class Repository:
  url = ''
  branch = ''

  def __init__(self, url, branch='master'):
    self.url = url
    self.branch = branch

  def pull(self, mode='full'):
    return steps.Git(
      repourl=self.url,
      mode='full',
      branch=self.branch
    )

class Builder:
  name = ''
  required_programs = {}
  steps = []
  excluded_programs = {}

  def __init__(self, name, required_programs, steps, excluded_programs = {}):
    self.name = name
    self.steps = steps
    self.required_programs = required_programs
    self.excluded_programs = excluded_programs

  def to_config(self, repository, workers):
    filtered_workers = [w.name for w in workers if w.meets_requirements(self.required_programs, self.excluded_programs) == True]
    factory = util.BuildFactory()

    factory.addStep(repository.pull())

    for step in self.steps:
      factory.addStep(step)

    return util.BuilderConfig(
      name=self.name,
      workernames=filtered_workers,
      factory=factory
    )


def os_builders(name, required_programs, oses, steps, excluded_programs = {}):
  return [Builder(name + '_' + o, [o] + required_programs, steps, excluded_programs) for o in oses]

class Project:
  name = ''
  builders = []

  def __init__(self, name, builders, repository):
    self.name = name
    self.builders = builders
    self.repository = repository

  def git_poller(self):
    return changes.GitPoller(
          self.repository.url,
          project=self.name,
          branch=self.repository.branch,
          pollinterval=300)

  def branch_change_scheduler(self):
    return schedulers.SingleBranchScheduler(
      name='changed_' + self.repository.branch + '_' + self.name,
      change_filter=util.ChangeFilter(
        branch=self.repository.branch,
        project=self.name
      ),
      treeStableTimer=None,
      builderNames=[b.name for b in self.builders]
    )

  def force_scheduler(self):
    return schedulers.ForceScheduler(
      name='force_' + self.name,
      builderNames=[b.name for b in self.builders]
    )

  def build_configs(self, workers):
    return [b.to_config(self.repository, workers) for b in self.builders]

repositories = {
  'cpp_loan_servicing': Repository('http://$IP:3000/mtolman/cpp_loan_servicing.git'),
  'cpp_calendar': Repository('http://$IP:3000/mtolman/calendar.git'),
  'cpp_cuid': Repository('http://$IP:3000/mtolman/cuid.git', 'main'),
}

builders = {
  'loan_servicing':
    os_builders('cpp_loan_servicing_static', ['cmake', 'ninja', 'git', 'c++'], ['alpine'], [
      steps.ShellCommand(command=['git', 'submodule', 'update', '--init', '--recursive']),
      steps.ShellCommand(command=['bash', 'build.sh', 'static']),
      steps.ShellCommand(command=['bash', 'verify.sh']),
      steps.ShellCommand(command=['bash', 'upload.sh', '$IP', '8081', 'buildbot:buildbot123'])
    ]) + os_builders('cpp_loan_servicing', ['cmake', 'ninja', 'git', 'c++'], ['ubuntu'], [
      steps.ShellCommand(command=['git', 'submodule', 'update', '--init', '--recursive']),
      steps.ShellCommand(command=['bash', 'build.sh']),
      steps.ShellCommand(command=['bash', 'verify.sh']),
      steps.ShellCommand(command=['bash', 'upload.sh', '$IP', '8081', 'buildbot:buildbot123'])
    ]),
  'cpp_calendar': os_builders('cxx_calendar', ['cmake', 'ninja', 'git', 'c++', 'llvm', 'lcov', 'clang-format', 'gcovr', 'clang-tidy', 'doxygen', 'graphviz'], ['ubuntu'], [
      steps.ShellCommand(command=['git', 'submodule', 'update', '--init', '--recursive']),
      steps.ShellCommand(command=['bash', 'build.sh']),
      steps.ShellCommand(command=['bash', 'verify.sh']),
      steps.ShellCommand(command=['bash', 'upload.sh', '$IP', '8081', 'buildbot:buildbot123'])
    ]) + os_builders('cxx_calendar_docs', ['make', 'latex', 'zip', 'cmake', 'doxygen', 'graphviz', 'docbook'], ['ubuntu'], [
      steps.ShellCommand(command=['git', 'submodule', 'update', '--init', '--recursive']),
      steps.ShellCommand(command=['bash', 'docs.sh', '$IP', '8081', 'buildbot:buildbot123']),
    ]),
  'cpp_cuid': os_builders('cxx_cuid', ['cmake', 'ninja', 'git', 'c++'], ['ubuntu', 'alpine'], [
      steps.ShellCommand(command=['git', 'submodule', 'update', '--init', '--recursive']),
      steps.ShellCommand(command=['bash', 'build.sh']),
      steps.ShellCommand(command=['bash', 'verify.sh']),
      steps.ShellCommand(command=['bash', 'upload.sh', '$IP', '8081', 'buildbot:buildbot123'])
    ])
}

projects = [
  Project('cpp_loan_servicing', builders['loan_servicing'], repositories['cpp_loan_servicing']),
  Project('cpp_calendar', builders['cpp_calendar'], repositories['cpp_calendar']),
  Project('cpp_cuid', builders['cpp_cuid'], repositories['cpp_cuid']),
]


workers = [
  Worker('ubuntu-cmake-worker-' + str(num + 1), {
    'ubuntu',
    'cmake',
    'c++',
    'c',
    'llvm',
    'lcov',
    'clang-format',
    'gcovr',
    'clang-tidy',
    'doxygen',
    'make',
    'graphviz',
    'ninja',
    'python3',
    'git',
    'gitleaks',
    'subversion'}) for num in range(""" + str(num_ubuntu_cmake_workers) + """)
  ] + [
    Worker('alpine-cmake-worker-' + str(num + 1), {
      'alpine',
      'cmake',
      'c++',
      'c',
      'ninja',
      'make',
      'python3',
      'cargo',
      'musl',
      'git',
      'subversion'}) for num in range(""" + str(num_alpine_cmake_workers) + """)
  ] + [
    Worker('arch-cmake-worker-' + str(num + 1), {
      'arch',
      'cmake',
      'c++',
      'c',
      'ninja',
      'make',
      'python3',
      'cargo',
      'git',
      'subversion'
    }) for num in range(""" + str(num_arch_cmake_workers) + """)
  ] + [
      Worker('document-worker-' + str(num + 1), {
        'ubuntu',
        'latex',
        'cmake',
        'make',
        'pandoc',
        'imagemagick',
        'naturaldocs',
        'graphviz',
        'Sphinx',
        'rst2pdf',
        'git',
        'subversion',
        'nodejs',
        'curl',
        'wget',
        'zip',
        'unzip',
        'tar',
        'docbook',
        'doxygen'}) for num in range(""" + str(num_document_workers) + """)
  ]


####### WORKERS

# The 'workers' list defines the set of recognized workers. Each element is
# a Worker object, specifying a unique worker name and password.  The same
# worker name and password must be configured on the worker.

def find_workers_with_requirements(needed = [], excluded = []):
  return [w for w in workers if w.meets_requirements(needed, excluded) == True]

c['workers'] = [w.to_build_bot_worker() for w in workers]

if 'BUILDBOT_MQ_URL' in os.environ:
    c['mq'] = {
        'type' : 'wamp',
        'router_url': os.environ['BUILDBOT_MQ_URL'],
        'realm': os.environ.get('BUILDBOT_MQ_REALM', 'buildbot').decode('utf-8'),
        'debug' : 'BUILDBOT_MQ_DEBUG' in os.environ,
        'debug_websockets' : 'BUILDBOT_MQ_DEBUG' in os.environ,
        'debug_lowlevel' : 'BUILDBOT_MQ_DEBUG' in os.environ,
    }
# 'protocols' contains information about protocols which master will use for
# communicating with workers. You must define at least 'port' option that workers
# could connect to your master with this protocol.
# 'port' must match the value configured into the workers (with their
# --master option)
c['protocols'] = {'pb': {'port': os.environ.get('BUILDBOT_WORKER_PORT', 9989)}}

####### CHANGESOURCES

# the 'change_source' setting tells the buildmaster how it should find out
# about source code changes.  Here we point to the buildbot clone of pyflakes.

c['change_source'] = [p.git_poller() for p in projects]

####### SCHEDULERS

# Configure the Schedulers, which decide how to react to incoming changes.  In this

c['schedulers'] = [p.branch_change_scheduler() for p in projects] + [p.force_scheduler() for p in projects]

# midnight
# c['schedulers'].append(
#     schedulers.Nightly(name='nightly',
#                        change_filter=util.ChangeFilter(branch='master'),
#                        builderNames=['cpp_loan_servicing_static', 'cpp_loan_servicing'],
#                        hour=0,
#                        minute=0))

####### BUILDERS

# The 'builders' list defines the Builders, which tell Buildbot how to perform a build:
# what steps, and which workers can execute them.  Note that any particular build will
# only take place on one worker.

c['builders'] = [config for project in projects for config in project.build_configs(workers)]


####### REPORTER TARGETS

# 'services' is a list of Reporter Targets. The results of each build will be
# pushed to these targets. buildbot/reporters/*.py has a variety to choose from,
# like IRC bots.

c['services'] = []

####### PROJECT IDENTITY

# the 'title' string will appear at the top of this buildbot installation's
# home pages (linked to the 'titleURL').

c['title'] = 'Buildbot: $HOSTNAME'
c['titleURL'] = 'http://$IP:8010/'

# the 'buildbotURL' string should point to the location where the buildbot's
# internal web server is visible. This typically uses the port number set in
# the 'www' entry below, but with an externally-visible host name which the
# buildbot cannot figure out without some help.

c['buildbotURL'] = os.environ.get('BUILDBOT_WEB_URL', 'http://$IP:8010/')

# minimalistic config to activate new web UI
c['www'] = dict(port=os.environ.get('BUILDBOT_WEB_PORT', 8010),
                plugins=dict(waterfall_view={}, console_view={}))

####### DB URL

c['db'] = {
    # This specifies what database buildbot uses to store its state.  You can leave
    # this at its default for all but the largest installations.
    'db_url' : os.environ.get('BUILDBOT_DB_URL', 'sqlite://').format(**os.environ),
}


c['buildbotNetUsageData'] = None"""


def write_compose_document_entry(file, num):
  file.write("""
  buildbot_document_""" + str(num) + """:
    build:
        context: .
        dockerfile: ubuntu-document.Dockerfile
    restart: unless-stopped
    environment:
        - WORKER_NAME=document-worker-""" + str(num) + """
    logging:
        driver: json-file
        options:
            max-size: "200k"
            max-file: "10"
    links:
      - buildbot_master:buildbot_master""")

def write_compose_ubuntu_cmake_entry(file, num):
  file.write("""
  buildbot_ubuntu_cmake_""" + str(num) + """:
    build:
        context: .
        dockerfile: ubuntu-cmake.Dockerfile
    restart: unless-stopped
    environment:
        - WORKER_NAME=ubuntu-cmake-worker-""" + str(num) + """
    logging:
        driver: json-file
        options:
            max-size: "200k"
            max-file: "10"
    links:
      - buildbot_master:buildbot_master""")

def write_compose_alpine_cmake_entry(file, num):
  file.write("""
  buildbot_alpine_cmake_""" + str(num) + """:
    build:
        context: .
        dockerfile: alpine-cmake.Dockerfile
    restart: unless-stopped
    environment:
        - WORKER_NAME=alpine-cmake-worker-""" + str(num) + """
    logging:
        driver: json-file
        options:
            max-size: "200k"
            max-file: "10"
    links:
      - buildbot_master:buildbot_master""")

def write_compose_arch_cmake_entry(file, num):
  file.write("""
  buildbot_arch_cmake_""" + str(num) + """:
    build:
        context: .
        dockerfile: arch-cmake.Dockerfile
    restart: unless-stopped
    environment:
        - WORKER_NAME=arch-cmake-worker-""" + str(num) + """
    logging:
        driver: json-file
        options:
            max-size: "200k"
            max-file: "10"
    links:
      - buildbot_master:buildbot_master""")


cfg_file = open("master/master.cfg", "w")
cfg_file.write(cfg_file_text)
cfg_file.close()

compose_file = open("docker-compose.yml", "w")
compose_file.write("""
version: "3.3"
services:
  buildbot_master:
    image: buildbot/buildbot-master:master
    env_file:
      - db.env
    environment:
      - BUILDBOT_CONFIG_DIR=/buildbot
      - BUILDBOT_WORKER_PORT=9989
      - BUILDBOT_WEB_URL=http://$IP:8010/
      - BUILDBOT_WEB_PORT=tcp:port=8010
    volumes:
      - "./master:/buildbot"
    links:
      - db
    depends_on:
      - db
    ports:
      - "8010:8010"
    logging:
        driver: json-file
        options:
            max-size: "200k"
            max-file: "10"
  db:
    env_file:
      - db.env
    image: "postgres:9.4"
    expose:
      - 5432
    logging:
        driver: json-file
        options:
            max-size: "200k"
            max-file: "10"
""")

for i in range(num_document_workers):
    write_compose_document_entry(compose_file, i + 1)

for i in range(num_ubuntu_cmake_workers):
    write_compose_ubuntu_cmake_entry(compose_file, i + 1)

for i in range(num_alpine_cmake_workers):
    write_compose_alpine_cmake_entry(compose_file, i + 1)

for i in range(num_arch_cmake_workers):
    write_compose_arch_cmake_entry(compose_file, i + 1)

compose_file.close()


f = open("alpine-cmake.Dockerfile", "w")
f.write("""FROM        alpine:3.14.0

# Update our main system
RUN apk update -qq && \
    apk upgrade && \
    apk add git build-base subversion \
        python3-dev make clang python3 \
        cmake dumb-init curl ninja openssl-dev \
        musl-dev cargo bash gcc libffi-dev

# Install python, pip, and python packages
RUN python3 -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools
RUN python3 -m pip install 'buildbot[bundle]'
RUN mkdir /buildbot

WORKDIR /buildbot
COPY start.sh /buildbot/start.sh
CMD bash /buildbot/start.sh""")
f.close()

f = open("arch-cmake.Dockerfile", "w")
f.write("""FROM        archlinux:base

RUN pacman-key --init

# Update our main system
RUN pacman -Syy

# Install packages
RUN pacman -S --noconfirm cmake python clang lld curl ninja make openssl git subversion

# Install python, pip, and python packages
RUN python -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools
RUN python -m pip install 'buildbot[bundle]'
RUN mkdir /buildbot

WORKDIR /buildbot
COPY start.sh /buildbot/start.sh
CMD bash /buildbot/start.sh""")
f.close()

f = open("ubuntu-cmake.Dockerfile", "w")
f.write("""FROM        ubuntu:20.04

# This will make apt-get install without question
ARG         DEBIAN_FRONTEND=noninteractive

# Update our main system
RUN apt-get update

# Install python, pip, and python packages
RUN apt-get install -yq python3 python3-pip
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install 'buildbot[bundle]'

# Install build tools
RUN apt-get install -y build-essential gcc clang llvm doxygen ninja-build graphviz libssl-dev curl git subversion wget

# Install CMAKE
RUN apt remove --purge cmake && \
    wget https://github.com/Kitware/CMake/releases/download/v3.22.0/cmake-3.22.0.tar.gz &&\
    tar -zxvf cmake-3.22.0.tar.gz && \
    cd cmake-3.22.0 &&\
    ./bootstrap &&\
    make -j 9 &&\
    make install &&\
    cd .. &&\
    rm -rf cmake-3.22.0

# Install coverage and analysis tools
RUN apt-get install -y lcov clang-format gcovr clang-tidy

# Install homebrew
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Install gitleaks
RUN brew install gitleaks cpplint

RUN mkdir /buildbot
WORKDIR /buildbot
COPY start.sh /buildbot/start.sh
CMD bash /buildbot/start.sh
""")
f.close()

f = open("ubuntu-document.Dockerfile", "w")
f.write("""FROM        ubuntu:20.04
# This will make apt-get install without question
ARG         DEBIAN_FRONTEND=noninteractive

# Update our main system
RUN apt-get update

# Install python, pip, and python packages
RUN apt-get install -yq python3 python3-pip
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install 'buildbot[bundle]'

# Install basic dev tools (so we can reuse basic dev systems for managing document building)
RUN apt-get install -y build-essential ninja-build make libssl-dev curl git subversion nodejs wget

# Install CMAKE
RUN apt remove --purge cmake && \
    wget https://github.com/Kitware/CMake/releases/download/v3.22.0/cmake-3.22.0.tar.gz &&\
    tar -zxvf cmake-3.22.0.tar.gz && \
    cd cmake-3.22.0 &&\
    ./bootstrap &&\
    make -j 9 &&\
    make install &&\
    cd .. &&\
    rm -rf cmake-3.22.0

# Install document tools
RUN apt-get install -y doxygen graphviz pandoc imagemagick naturaldocs make zip unzip tar xsltproc docbook-xsl fop docbook-xsl-doc-pdf docbook
RUN pip install Sphinx

# Install latex
RUN apt-get install -y texlive-full

RUN mkdir /buildbot
WORKDIR /buildbot
COPY start.sh /buildbot/start.sh
CMD bash /buildbot/start.sh""")
f.close()

f = open("db.env", "w")
f.write("""# database parameters are shared between containers
POSTGRES_PASSWORD=change_me
POSTGRES_USER=buildbot
POSTGRES_DB=buildbot
# in master.cfg, this variable is str.format()ed with the environment variables
BUILDBOT_DB_URL=postgresql+psycopg2://""" + "{" + "POSTGRES_USER}:{" + "POSTGRES_PASSWORD}@db/{" + """POSTGRES_DB}

""")
f.close()

f = open("start.sh", "w")
f.write("""
if [[ ! -d \"/buildbot/\$WORKER_NAME\" ]]; then
  buildbot-worker create-worker \$WORKER_NAME buildbot_master \$WORKER_NAME pass
fi
buildbot-worker start \$WORKER_NAME && tail -f \$WORKER_NAME/twistd.log
""")
f.close()

EOF

  cat docker-compose.yml | sed "s/localhost/$IP/g" > docker-compose.yml
  sudo mkdir -p /var/buildbot
  sudo chmod -R a+rw /var/buildbot
  cp create-docker-compose.py /var/buildbot/
  DIR=$(pwd)

  cd /var/buildbot || exit 1

  pip3 install 'buildbot[bundle]'
  sudo mkdir -p /var/buildbot/master
  sudo chmod -R a+rw /var/buildbot
  cd /var/buildbot/master || exit 1
  buildbot create-master .
  cd /var/buildbot/ || exit 1
  python3 create-docker-compose.py

  cd "$DIR" || exit 1

  cat << EOF > /var/buildbot/master/buildbot.tac
import os

from twisted.application import service
from buildbot.master import BuildMaster

basedir = '.'

rotateLength = 10000000
maxRotatedFiles = 10
configfile = 'master.cfg'

# Default umask for server
umask = None

# if this is a relocatable tac file, get the directory containing the TAC
if basedir == '.':
    basedir = os.path.abspath(os.path.dirname(__file__))

# note: this line is matched against to check that this is a buildmaster
# directory; do not edit it.
application = service.Application('buildmaster')
from twisted.python.logfile import LogFile
from twisted.python.log import ILogObserver, FileLogObserver
logfile = LogFile.fromFullPath(os.path.join(basedir, "twistd.log"), rotateLength=rotateLength,
                                maxRotatedFiles=maxRotatedFiles)
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)

m = BuildMaster(basedir, configfile, umask)
m.setServiceParent(application)
m.log_rotation.rotateLength = rotateLength
m.log_rotation.maxRotatedFiles = maxRotatedFiles
EOF

  cat << EOF > buildbot.service
[Unit]
Description=Docker Compose Application Service
After=syslog.target
After=network.target

[Service]
WorkingDirectory=/var/buildbot
ExecStart=/usr/bin/docker-compose up
ExecStop=/usr/bin/docker-compose down
TimeoutStartSec=2
Restart=on-failure
StartLimitIntervalSec=60
StartLimitBurst=3

[Install]
WantedBy=multi-user.target
EOF
  sudo mv buildbot.service /etc/systemd/system

  cd /var/buildbot || exit 1
  sudo docker-compose build
  cd "$DIR" || exit 1

  sudo systemctl enable buildbot.service
  sudo systemctl start buildbot.service

  printf '\nBuildbot Installed!\n'
}


##########################
## Buildbot Setup
##########################


ftp_setup() {
  sudo apt-get install -y vsftpd
  cat << EOF > vsftpd.conf
  listen=NO
  listen_ipv6=YES
  anonymous_enable=NO
  local_enable=YES
  write_enable=YES
  dirmessage_enable=YES
  use_localtime=YES
  xferlog_enable=YES
  connect_from_port_20=YES
  secure_chroot_dir=/var/run/vsftpd/empty
  pam_service_name=vsftpd
  rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
  rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
  ssl_enable=NO

EOF
  sudo mv vsftpd.conf /etc/vsftpd.conf
  sudo systemctl restart vsftpd
}


print_help() {
    echo "bash setup.sh [-h] [-d] [-g] [-n] [-e] [-j] [-p] [-b] [-f]
    -h  Show this message and exit
    -d  Installs docker
    -g  Installs Gitea (port: 3000)
    -n  Installs Nexus Repository (port: 8081)
    -e  Installs Compiler explorer (port: 10240)
    -j  Installs Jupyter (port: 9650)
    -p  Installs Plex (port: 32400)
    -b  Installs Buildbot (port: 8010)
    -f  Installs SFTP Server (port: 22)
    
Note: at least one flag must be passed in for system changes to be made"
    
    exit 0
}


if [[ -z "$1" ]]; then
  print_help
fi

while getopts 'hdgnjpbfc' OPTION; do
    case "$OPTION" in
        h) 
            print_help
            ;;
        d)
            docker_setup
            ;;
        g)
            gitea_setup
            ;;
        j)
            jupyter_setup
            ;;
        n)
            nexus_setup
            ;;
        c)
            compiler_explorer_setup
            ;;
        p)
            plex_setup
            ;;
        b)
            buildbot_setup
            ;;
        f)
            ftp_setup
            ;;
        *)
            print_help
            ;;
    esac
done
